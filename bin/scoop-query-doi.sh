#!/bin/bash
# This script queries https://doi.org about the single DOI given
# and retrieves a bibtex file with the result.

# Define the usage function
function usage() {
	echo "Usage: $(basename $0) [--help] [OPTIONS] DOI"
	echo
	echo "will query https://doi.org about the single DOI and return"
	echo "the result to stdout in bibtex format."
	echo 
	echo "OPTIONS can be"
	echo "  --raw     does not format the result"
	echo
	echo "Examples:"
	echo "  $(basename $0) 10.1002/gamm.201010013"
	echo "  $(basename $0) https://doi.org/10.1002/gamm.201010013"
	echo "  echo 10.1002/gamm.201010013 | $(basename $0)"
	echo 
}

# Set debugging flag
_DEBUG=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
 [ "$_DEBUG" = "true" ] && $@
}

# Set the default options
RAW=false

# Parse the command line arguments
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# https://www.assertnotmagic.com/2019/03/08/bash-advanced-arguments/
while (( "$#" )); do
	case "$1" in
		--raw)
			RAW=true
			shift
			;;
		
		--help|-*)
			usage
			exit 1
			;;
		
		*)
			# An argument starting with anything but '-' represents a DOI.
			# Only one such argument can be given.
			if [ -z "${DOI+x}" ]; then
				DOI="$1"
				DEBUG echo $DOI
				shift
			else
				usage
				exit 1
			fi
			;;

	esac
done

# Make sure we have a DOI, either from the command line, or from stdin
if [ -t 0 ]; then
	if [ -z "$DOI" ]; then
		usage
		exit 1
	fi
else
	DOI=$(cat -)
fi

# Extract the DOI (e.g., in case given in the form https://doi.org/10.1002/gamm.201010013)
# https://www.crossref.org/blog/dois-and-matching-regular-expressions/
DOIPARSED=$(echo "$DOI" | egrep '10[.][0-9]{4,9}/[-._;()/:a-zA-Z0-9]+' -o)
DEBUG echo "$DOI"
DEBUG echo "$DOIPARSED"
if [ -z "$DOIPARSED" ]; then
	echo "$DOI does not appear to be a valid DOI."
	exit 1
fi

# Make sure curl is available
if [ ! -x "$(command -v curl)" ]; then
	echo "$0 depends on curl, which does not seem to be available on your system."
	echo "Please install curl."
	exit 1
fi

# Create a temporary file to store the combined output of all queries
OUTFILE=$(mktemp).bib

# Retrieve the bibtex file
# BIBTEX=$(wget -O ${OUTFILE} --header="Accept: application/x-bibtex" --user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64)" https://doi.org/${DOIPARSED} 2>/dev/null)
curl --location --header "Accept: application/x-bibtex" --user-agent "Mozilla/5.0" "https://doi.org/$DOIPARSED" -o ${OUTFILE} 2>/dev/null

# Canonicalize the output.
scoop-canonicalize-bibliography.sh "$OUTFILE" | sponge "$OUTFILE"

# Read the bibtex file into a variable.
BIBTEX=$(<"$OUTFILE")
DEBUG echo "${BIBTEX}"

# Check for valid output.
if [[ ${BIBTEX:0:1} != "@" ]]; then
	>&2 echo "Query about $DOI to https://doi.org not successful."
	exit 1
fi

# Define a filter which will throw away the URL in case it contains a DOI.
PROGNOURL='/^\s*URL\s*=\s*{https?:\/\/(dx\.)?doi\.org/ { next; } { print; }'

# Define a filter which will throw away the LANGUAGE entry
PROGNOLANGUAGE='/^\s*LANGUAGE\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the COPYRIGHT entry
PROGNOCOPYRIGHT='/^\s*COPYRIGHT\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the KEYWORDS entry
PROGNOKEYWORDS='/^\s*KEYWORDS\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the ABSTRACT entry
PROGNOABSTRACT='/^\s*ABSTRACT\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the ISSN and ISBN
PROGNOISSNISBN='/^\s*(ISSN|ISBN)\s*=/ { next; } { print; }'

# Due to an issue with crossref.org, see 
# https://gitlab.com/crossref/issues/-/issues/1540,
# the journal field is missing for records of type @ARTICLE.
# We retrieve this field by other means and insert it.
CONTAINER=$(curl -s "https://api.crossref.org/works/$DOIPARSED" | jq -M '.message."container-title" | .[]' | cut -f2 -d\")
DEBUG echo CONTAINER follows:
DEBUG echo $CONTAINER
DEBUG echo
INSERT=""

# For an @article type, use the content of $CONTAINER as the JOURNALTITLE, unless
# JOURNALTITLE is already set.
if [[ $BIBTEX == @ARTICLE ]]; then
	if [[ $BIBTEX =~ JOURNALTITLE ]]; then
		continue
	else
		INSERT="JOURNALTITLE = {"$CONTAINER"},"
		INSERT=${INSERT//$'\n'/}
		INSERT=${INSERT//$'\r'/}
		DEBUG echo String to be inserted follows:
		DEBUG echo $INSERT
		DEBUG echo
	fi
fi

# For an @INCOLLECTION type, use the content of $CONTAINER as the JOURNALTITLE, unless
# JOURNALTITLE is already set.
if [[ $BIBTEX == @INCOLLECTION ]]; then
	if [[ $BIBTEX =~ BOOKTITLE ]]; then
		continue
	else
		INSERT="BOOKTITLE = {"$CONTAINER"},"
		INSERT=${INSERT//$'\n'/}
		INSERT=${INSERT//$'\r'/}
		DEBUG echo String to be inserted follows:
		DEBUG echo $INSERT
		DEBUG echo
	fi
fi

# Insert the string into the second line of the BIBTEX string.
if [ "$RAW" = "true" ]; then
	continue
else
	if [[ ! -z $INSERT ]]; then
		DEBUG echo Now inserting:
		DEBUG echo $INSERT
		BIBTEX=$(echo "$BIBTEX" | sed "2i${INSERT}")
		DEBUG echo Done
		DEBUG echo
	fi
fi

# Dump the result, then apply additional filters.
if [ "$RAW" = "true" ]; then
	echo "${BIBTEX}"
else
	echo "${BIBTEX}" | awk "$PROGNOURL" | awk "$PROGNOLANGUAGE" | awk "$PROGNOCOPYRIGHT" | awk "$PROGNOKEYWORDS" | awk "$PROGNOABSTRACT" | awk "$PROGNOISSNISBN"
fi

