# This awk script abbreviates given names, preserving only the first letter for each given name,
# typically of an author or editor in a .bib file. A format as in
#   AUTHOR = {Hoffmann, Karl-Heinz and Hoppe, Ronald H. W. and Schulz, Volker},
# with 'and' separated names is assumed.
#
# Input variables:
#
#   field   specifies a regular expression used to finding fields to consider for abbreviation
#           Example: "AUTHOR"
#
# This script takes no file arguments and it is called as in
#   awk -f scoop-abbreviate-given-names.awk -v "field=AUTHOR" scoop.bib

BEGIN { 
	# Define the regular expression to determine whether the field matches and how to split the line
	LineRegex = "^([ ]*" field " = {)(.*)(},)"
}

{
	# Split matching lines into givenNameParts, e.g.,
	#   EDITOR = {Hoffmann, Karl-Heinz and Hoppe, Ronald H. W. and Schulz, Volker},
	# will result in 
	#   lineParts[1] = 'EDITOR = {'
	#   lineParts[2] = 'Hoffmann, Karl-Heinz and Hoppe, Ronald H. W. and Schulz, Volker'
	if (match($0, LineRegex, lineParts)) 
	{ 
		# Output the first part 
		printf "%s",lineParts[1]; 
		# Split the middle part at 'and' into the 'names' array
		nNames = split(lineParts[2], names, " and ");  
		# Process each name
		for (iterNames = 1; iterNames <= nNames; iterNames++) 
		{ 
			# Split the name at ','
			split(names[iterNames], lastNamesGivenNames, ",");  
			# Output the last name(s)
			printf "%s, ",lastNamesGivenNames[1]; 
			# Split the given names at ' ' and '-'
			nParts = split(lastNamesGivenNames[2], givenNameParts, "[- ]", separators); 
			# Process each given name
			# Since givenNameParts starts with an empty field followed by separator ' ', 
			# we start at field #2 
			for (iterGivenNameParts = 2; iterGivenNameParts <= nParts; iterGivenNameParts++) 
			{
				# Print the given name reduced to (in most cases) the first letter followed by '.' and the separator
				if (givenNameParts[iterGivenNameParts] ~ /^[^\\]/)
				{
					printf "%s.%s",substr(givenNameParts[iterGivenNameParts],1,1),separators[iterGivenNameParts]; 
				}
				# However, if the given name starts with '\', then copy it until we have found at least one '{' and then until the matching '}'
				# Since awk does not allow non-greedy matches, we have to parse the string from left to right
				# Examples are '\.{I}lker' but also the fictitious '\v{R}\'{\i{}}'
				else
				{
					numberOfOpeningBrackets = 0;
					numberOfClosingBrackets = 0;
					for (iterChar = 1; iterChar <= length(givenNameParts[iterGivenNameParts]); iterChar++)
					{
						if (substr(givenNameParts[iterGivenNameParts], iterChar, 1) == "{") numberOfOpeningBrackets++;
						if (substr(givenNameParts[iterGivenNameParts], iterChar, 1) == "}") numberOfClosingBrackets++;
						if ((numberOfOpeningBrackets > 0) && (numberOfOpeningBrackets == numberOfClosingBrackets)) break;
					}
					printf "%s.%s",substr(givenNameParts[iterGivenNameParts],1,iterChar),separators[iterGivenNameParts];
				}
			} 
			# If more names are coming, output an ' and '
			if (iterNames < nNames) printf " and "; 
		} 
		# Output the last part of the line
		printf "%s\n",lineParts[3]
	} 
	else 
	{ 
		# If the line does not match, print it verbatim
		print $0; 
	} 
}
