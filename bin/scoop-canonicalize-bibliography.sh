#!/bin/bash
# This script applies essentially
#   biber --tool
# in order to apply certain canonicalization rules to a BibLaTeX file
# and dump it to stdout. The goal is to
# * have author, editor names in the form surname(s), given name(s)
# * keep only fields consistent with the data model, e.g.,
#   purge the MRREVIEWER field etc.
# Moreover, unnecessary braces {} will be removed. TODO

# Issue help message if necessary
if [ $# = 0 ] && [ -t 0 ] || [ "x$1" = "x--help" ]; then
	echo "Usage: $(basename $0) file.bib"
	echo "       cat file.bib | $(basename $0)"
	echo
	echo "will canonicalize the entries in the BibLaTeX file.bib and dump"
	echo "the result to stdout. Cite keys will not be altered."
	echo "Extra braces {} will be removed."
	echo
	echo "Examples:"
	echo "  $0 scoop.bib"
	echo "  $0 scoop.bib | sponge scoop.bib"
	echo
	exit 1
fi

# Make sure the input file exists and is readable
# (unless running with piped stdin)
if [ -t 0 ] && [[ ! -r "$1" ]]; then
	echo "Input file $1 not found."
	exit 1
fi

# Make sure the config exists and is readable
# The configuration file is generated using
#   BIBERVERSION=$(biber --version | grep -o '[0-9.]*')
#   cp $(biber --tool-config) biber-tool-${BIBERVERSION}.conf
#   cp biber-tool-${BIBERVERSION}.conf scoop-biber-tool-${BIBERVERSION}.conf
# Then either edit scoop-biber-tool-${BIBERVERSION}.conf to create a custom
# .conf file by hand, using the diff between the default and the custom .conf
# files of a previous version as template,
#   diff biber-tool-2.14.conf scoop-biber-tool-2.14.conf
# or better use a patch to apply the diff and verify the results:
#   diff --unified biber-tool-2.14.conf scoop-biber-tool-2.14.conf | patch --no-backup-if-mismatch --merge scoop-biber-tool-${BIBERVERSION}.conf
# Check scoop-biber-tool-${BIBERVERSION}.conf for conflict markers.
CONFIGFILE="$(dirname "$0")/scoop-biber-tool-2.17.conf"
if [[ ! -r "$CONFIGFILE" ]]; then
	echo "Config file $CONFIGFILE not found."
	exit 1
fi

# biber apparently cannot read from stdin, so we need to dump stdin to a temporary file if required
if [ $# -ge 1 ]; then
	INFILE="$1"
else
	INFILE=$(mktemp)
	sed -n "w $INFILE"
fi

# Launch biber to do the canonicalization according to the rules set forth in the config file
BIBTEX=$(biber --tool --quiet --output-file - --configfile "$CONFIGFILE" "$INFILE")

# Unexpectedly, the above call to biber generates an empty file '-' in addition to
# the results being dumped to stdout. We remove it.
rm -f -- '-'

# Remove extra pairs of braces {}
BIBTEX=$(echo "$BIBTEX" | awk -f $(dirname $0)/scoop-remove-extra-braces.awk)

# Dump the final outcome
echo "$BIBTEX"

