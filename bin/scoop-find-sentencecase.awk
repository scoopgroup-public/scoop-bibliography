# This awk script finds sentencecase in given fields of entries of given type (typically in a .bib file). 
# Its intention is to detect incorrect capitalization, e.g., in the TITLE field of BOOKs.
# Lines with more than one lower case word (exceptions removed) will be reported.
#
# Input variables:
#
#   entry   specifies a regular expression used for finding entries of given type
#           Example: "@BOOK"
#   field   specifies a regular expression used to finding fields to consider for sentence case search
#           Example: " TITLE"
#
# This script takes two file arguments, as in
#   awk -f scoop-find-sentencecase.awk -v "entry=@BOOK" -v "field= TITLE" scoop-lowercase-words.txt scoop.bib
# The first file argument contains words known to require lower case.
# The second file argument is the .bib file to be processed.

BEGIN { 
	# Define the regular expression to determine sentence case
	SentenceCaseRegex = "(\\<[a-z].*){1,}"
}

# If we are reading the file with exceptions, build a regular expression
# https://www.unix.com/shell-programming-and-scripting/197183-awk-delete-word-when-found.html
{
	if (FNR == NR) 
	{
		# Append the current line to the regular expression 
		wordsRequiringLowerCase = (wordsRequiringLowerCase?wordsRequiringLowerCase"|":"") $0 
		next
	}
}

# When a new entry is found, determine whether or not to process it
/^@/ {
	if ($0 ~ entry) { processThisEntry = 1 }
	else { processThisEntry = 0 }
}

# In case the entry is being processed, the field matches, and 
# title case is determined, print the line
{
	if (processThisEntry == 1)
	{
		# Remove words recognized as needing capitalization from the line
		lineWithExceptionsRemoved = $0
		gsub(wordsRequiringLowerCase, "", lineWithExceptionsRemoved)
		if (($0 ~ field) && (lineWithExceptionsRemoved ~ SentenceCaseRegex)) { print $0 }
	}
}

