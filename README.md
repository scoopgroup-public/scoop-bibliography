# scoop-bibliography

This repository provides the main bibliography `scoop.bib` maintained and used by the SCOOP group, Heidelberg University.
It includes a number of scripts in the folder `bin/` to amend and manipulate the bibliography file.
The scripts are written in `python` or for the shell (`bash`).

# Workflow
## Obtaining BibLaTeX entries
The `scoop.bib` file should **not be edited by hand**.
The following workflow describes the addition of entries in an automated way, by example.
To this end, we have a universal `scoop-get-bib.sh` retrieval script.
1. Query (as an example) arXiv for a publication by arXiv identifier:
```bash
scoop-get-bib.sh 1709.01343
```
2. Check the outcome for obvious errors.
Notice that the citation key is temporary and will be set automatically when merging with an existing `.bib` file.
3. If you are happy with the outcome, you may merge the query result into the target file, e.g., `scoop.bib`:
```bash
scoop-get-bib.sh 1709.01343 --edit --merge ~/Work/public/scoop-bibliography/scoop.bib
```
This will assign the final citation key, and call `scoop-merge-bibliography.py` to merge, avoiding duplicate entries.

In some cases, a query results in a slightly incorrect BibLaTeX code, e.g., `scoop-get-bib.sh 1902.07240` will be confused about the given and family names of the author `José Vidal Núñez` and will incorrectly report it as `Núñez, José Vidal` in the `AUTHOR` field.
In this case, use
```bash
scoop-get-bib.sh 1902.07240 --merge ~/Work/public/scoop-bibliography/scoop.bib --edit
```
to allow yourself to edit the results before the merge.

## Description of bibliography manipulation tools
In the following we describe all of our tools related to bibliography manipulation.
They will be listed here roughly in decreasing order of relevance to the average user.

### scoop-get-bib.sh
This is a universal citation retrieval script which currently supports queries to
* https://arxiv.org
* https://www.crossref.org
* https://mathscinet.ams.org/mathscinet
The latter requires `mathscinet.pl` from https://github.com/gerw/mathbin, which is packaged as a submodule to `scoop-bibliography` but can also be obtained independently.
Notice that queries to [MathSciNet](https://mathscinet.ams.org/mathscinet) requires a subscription and may be available to you only through a VPN connection.

Notice that multiple queries can be passed in one go, such as
```bash
scoop-get-bib.sh -a herzog -t "superlinear+convergence" 1709.01343 1902.07240 10.1088/1361-6420/ab6d5b
```
See `scoop-get-bib.sh --help` for details.

### scoop-query-arxiv.sh, scoop-query-doi.sh, scoop-query-mathscinet.sh
These scripts are used internally by `scoop-get-bib.sh` but can be called also by the user.
They will specifically query
* https://arxiv.org
* https://www.crossref.org
* https://mathscinet.ams.org/mathscinet
respectively, for the identifier or search pattern given.
Use `--help` to get more information.

### scoop-merge-bibliography.py
The call
```
scoop-merge-bibliography.py new.bib old.bib
```
will merge the contents of `new.bib` into `old.bib`.
Duplicates will be recognized by way of
* arXiv identifier (in fact, the EPRINT field)
* DOI
and will be dropped.
`scoop-merge-bibliography.py` does not alter citation keys.

### scoop-generate-citekeys.py
The command
```
scoop-generate-citekeys.py new.bib
```
will generate temporary citation keys for the entries in `new.bib`.

### scoop-finalize-keys.sh

The call
```
scoop-finalize-keys.sh scoop.bib
```
replace any temporary citation keys in `scoop.bib` by permanent ones, ensuring appropriate numbering.

### scoop-canonicalize-bibliography.sh
The command

```
scoop-canonicalize-bibliography.sh scoop.bib
```
processes `scoop.bib` through various filters and
* sorts entries by citation key
* purges superfluous fields (such as MRREVIEWER)
* sorts fields alphabetically
* removes extra curly braces (not required by BibLaTeX

### scoop-validate-bibliography.sh
The command
```
scoop-validate-bibliography.sh scoop.bib
```
performs a number of validation checks on `scoop.bib` including:
* duplicate citation keys
* citation keys not following the standard format
* extra curly braces
* abbreviated journal names
* sentence and title case

# Prerequisites
## Checking python requirements
The python scripts require
* python3 (lowest tested version: 3.6.9)
* pybtex (lowest tested version: 0.22.2)
```bash
for module in pybtex; do
  if ! pip3 list --format=columns | grep $module; then
    echo "$module MISSING"
    echo "Try: pip3 install $module"
  fi
done
```
## Checking bash requirements
```bash
for prog in awk biber cat curl diff gawk grep perl recode sed sponge wget xmllint; do
  command -v $prog >/dev/null 2>&1 || echo "Command $prog is MISSING";
done
```
### Installation hints
* `sponge` is part of [moreutils](https://joeyh.name/code/moreutils/) and is available as a linux package.

### Mac OS
On Mac OS you can use [Homebrew](https://brew.sh/) to install linux packages.
For instance, for the package `moreutils`, say `brew install moreutils`.

