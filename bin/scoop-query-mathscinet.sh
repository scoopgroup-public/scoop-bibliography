#!/bin/bash
# This script queries https://mathscinet.ams.org/mathscinet about the publication(s)
# matching the given search criteria and retrieves a bibtex file with the result(s).

# Define the usage function
function usage() {
	echo "Usage: $(basename $0) [--help] [QUERY] [OPTIONS]"
	echo
	echo "will query (a mirror of) https://mathscinet.ams.org/mathscinet about the publications(s)"
	echo "matching the given search criteria and return the result to stdout in bibtex format."
	echo 
	echo "QUERY can be"
	echo "  -a, --author" 
	echo "  -j, --journal"
  echo "  -m, --MR"
	echo "  -s, --series"
	echo "  -t, --title" 
	echo "  -y, --year"
	echo
	echo "OPTIONS can be"
	echo "  --raw             does not format the result"
	echo
	echo "Examples:"
	echo "  $(basename $0) -t \"free+groups\" -t trees -a bestvina -y 1997"
	echo "  $(basename $0) -a serre -j annals -y 1955-"
	echo "  $(basename $0) -a brown -s \"graduate+texts\" -y -2000"
	echo 
}

# Set debugging flag
_DEBUG=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
 [ "$_DEBUG" = "true" ] && $@
}

# Set the default options
RAW=false

# Parse the command line arguments
# https://stackoverflow.com/questions/21542054/how-to-intercept-and-remove-a-command-line-argument-in-bash
declare -a ARGS
for var in "$@"; do
	# Discover --help command line argument
	if [ "$var" = '--help' ]; then
		usage
		exit 1
	fi
	# Remove --raw command line argument from arguments to be passed to mathscinet.pl
	if [ "$var" = '--raw' ]; then
		RAW=true
		continue
	fi
	# Collect all other command line arguments in ARGS
	ARGS[${#ARGS[@]}]="$var"
done

# Make sure mathscinet.pl is available
MATHSCINET="mathscinet.pl"
if [ ! -x "$(command -v ${MATHSCINET})" ]; then
  # Provide the script from our sub module only if it was checked out
  # Require the script dir, see https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
  MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  MATHSCINET="${MYDIR}/../mathbin/mathscinet.pl"
  if [ ! -x "$(command -v ${MATHSCINET})" ]; then
    echo "$0 depends on ${MATHSCINET}, which does not seem to be available on your system."
    echo "Please get it from https://github.com/gerw/mathbin or initialize the submodules."
    exit 1
  fi
fi

# Retrieve the bibtex file
DEBUG echo ${MATHSCINET} "${ARGS[@]}" 
BIBTEX=$(${MATHSCINET} "${ARGS[@]}")

# Define a filter which will throw away the URL in case it contains a DOI
PROGNOURL='/^\s*URL\s*=\s*{https?:\/\/(dx\.)?doi\.org/ { next; } { print; }'

# Define a filter which will throw away mathscinet related content
PROGNOMR='/^\s*(MRNUMBER|MRREVIEWER|MRCLASS)\s*=/ { next; } { print; }'

# Define a filter which will throw away the JOURNAL entry (abbreviated journal name)
PROGNOJOURNAL='/^\s*JOURNAL\s*=/ { next; } { print; }'

# Define a filter which will rename the FJOURNAL entry by JOURNAL
PROGFJOURNAL='/^\s*FJOURNAL\s*=/ { gsub("FJOURNAL", " JOURNAL"); } { print; }'

# Define a filter which will throw away the ISSN and ISBN
PROGNOISSNISBN='/^\s*(ISSN|ISBN)\s*=/ { next; } { print; }'

# Define a filter which will throw away the LANGUAGE entry
PROGNOLANGUAGE='/^\s*language\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the COPYRIGHT entry
PROGNOCOPYRIGHT='/^\s*copyright\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the KEYWORDS entry
PROGNOKEYWORDS='/^\s*keywords\s*=\s*{/ { next; } { print; }'

# Define a filter which will throw away the ABSTRACT entry
PROGNOABSTRACT='/^\s*abstract\s*=\s*{/ { next; } { print; }'

# Dump the result, then apply additional filters
if [ "$RAW" = "true" ]; then
	echo "$BIBTEX"
else
	echo "$BIBTEX" | awk "$PROGNOURL" | awk "$PROGNOMR" | awk "$PROGNOJOURNAL" | awk "$PROGFJOURNAL" | awk "$PROGNOISSNISBN" | awk "$PROGNOLANGUAGE" | awk "$PROGNOCOPYRIGHT" | awk "$PROGNOKEYWORDS" | awk "$PROGNOABSTRACT"
fi

