# This awk script finds entries with missing digital identifiers, such as DOIs and arXiv identifiers.
#
# Input variables:
#
#   entry   specifies a regular expression used for finding entries of given type
#           Example: "@BOOK"
#   field   specifies a regular expression used to finding fields required to pass
#           Example: " TITLE"
#
# This script takes two file arguments, as in
#   awk -f scoop-find-missing-identifiers.awk -v "entry=@ARTICLE" -v "field=DOI|URL" scoop-identifier-exceptions.txt scoop.bib
# The first file argument contains citation keys which are exempted from the test for missing identifiers.
# The second file argument is the .bib file to be processed.

BEGIN { 
}

# If we are reading the file with exceptions, build a regular expression
# https://www.unix.com/shell-programming-and-scripting/197183-awk-delete-word-when-found.html
{
	if (FNR == NR) 
	{
		# Append the current line to the regular expression 
		keysExemptedFromTesting = (keysExemptedFromTesting?keysExemptedFromTesting"|":"") $0 
		next
	}
}

# When a new entry is found, determine whether or not to process it
/^@/ {
	if (($0 ~ entry) && ($0 !~ keysExemptedFromTesting)) { processThisEntry = 1; currentKey = $0; }
	else { processThisEntry = 0 }
}

# In case the entry is being processed and an identifier field is found, skip to the next entry
{
	if ((processThisEntry == 1) && ($0 ~ field)) { processThisEntry = 0 }
}

# In case the entry is being processed and its end is found, report it
{
	if ((processThisEntry == 1) && ($0 ~ /}$/)) { print currentKey }
}

